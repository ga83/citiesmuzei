package tux.com.citiesmuzei;

import android.content.Context;
import android.graphics.Point;
import android.net.Uri;
import android.util.Log;
import android.view.WindowManager;

import com.google.android.apps.muzei.api.provider.Artwork;
import com.google.android.apps.muzei.api.provider.MuzeiArtProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class WorldCitiesArtProvider extends MuzeiArtProvider {

    private final String TAG = this.getClass().toString();
    private static String IP = "8.8.8.8";
    private long READ_TIMEOUT = 10 * 1000;
    private long WRITE_TIMEOUT = 10 * 1000;

    public WorldCitiesArtProvider() {

    }

    @Override
    protected void onLoadRequested(boolean initial) {
        Artwork artwork = getNewArtwork();

        if(artwork != null) {
            addArtwork(artwork);
        }
    }


    private com.google.android.apps.muzei.api.provider.Artwork getNewArtwork() {
        Context context = getContext();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Point size = new Point();
        wm.getDefaultDisplay().getRealSize(size);
        int width = size.x;
        int height = size.y;

        HttpUrl.Builder urlBuilder = HttpUrl.parse("http://" + IP + "/randomphoto.php").newBuilder();
        urlBuilder.addQueryParameter("width", String.valueOf(width));
        urlBuilder.addQueryParameter("height", String.valueOf(height));

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder = configureToIgnoreCertificate(builder);

        OkHttpClient client = builder
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS)
                .build();

        Response response = null;
        String jsonResponse = "nothing";
        JSONObject jsonObject = null;

        boolean success = false;
        String imageUrl = "";
        String imageTitle = "";
        String imageOwner = "";
        String imageContextUrl = "";
        Uri uri = null;
        Uri contextUri = null;

        try {
            response = client.newCall(request).execute();
            jsonResponse = response.body().string();

            if(jsonResponse.contains("imageUrl") == false) {
                throw new WorldCitiesNetworkException("imageUrl key not present");
            }

            jsonObject = new JSONObject(jsonResponse);
            imageUrl = jsonObject.getString("imageUrl");
            imageTitle = jsonObject.getString("city") + ", " + jsonObject.getString("country");
            imageOwner = jsonObject.getString("imageOwner");
            imageContextUrl = jsonObject.getString("imageContextUrl");

            if(imageUrl.equals("") == true) {
                throw new WorldCitiesNetworkException("imageUrl value empty");
            }

            uri = Uri.parse(imageUrl);
            contextUri = Uri.parse(imageContextUrl);
            success = true;
        }
        catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
            e.printStackTrace();
        }
        catch (JSONException e) {
            Log.e(TAG, "JSONException: " + e.getMessage());
            e.printStackTrace();
        }
        catch (WorldCitiesNetworkException e) {
            Log.e(TAG, "WorldCitiesNetworkException: " + e.getMessage());
            e.printStackTrace();
        }

        if (success == true) {
            Artwork artwork = new Artwork
                    .Builder()
                    .title(imageTitle)
                    .byline(imageOwner)
                    .persistentUri(uri)
                    .webUri(contextUri)
                    .build();

            return artwork;
        }
        else {
            return null;
        }
    }

    private static OkHttpClient.Builder configureToIgnoreCertificate(OkHttpClient.Builder builder) {
        try {
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType)
                                throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType)
                                throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager)trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
        } catch (Exception e) {
            System.out.println("Exception while configuring IgnoreSslCertificate" + e);
        }
        return builder;
    }

    private class WorldCitiesNetworkException extends Throwable {
        String errorMessage;

        public WorldCitiesNetworkException(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        @Override
        public String getMessage() {
            return errorMessage;
        }
    }
}
